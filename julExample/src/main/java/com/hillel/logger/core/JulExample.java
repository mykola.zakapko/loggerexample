package com.hillel.logger.core;

import com.hillel.logger.entity.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Stream;


public class JulExample
{

    static {
        String path = JulExample.class.getClassLoader()
                .getResource("logging.properties")
                .getFile();
        System.setProperty("java.util.logging.config.file", path);
    }
    private static final Logger logger = Logger.getLogger(JulExample.class.getName());


    public static void main(String[] args) {
        User user = new User();
        user.setName("Peter");
        user.setLastName("Pupkin");

        logger.log(Level.INFO, "Application started and constructed with log level.");
        logger.info(user.showMeMessage());
//        Error because not a string
//        logger.info(user.giveMeASign());

        logger.info("Application started and constructed without set log level.");
        logger.warning("Something to warn");
        logger.severe("Something failed.");

        logger.info("Root Logger: "  + user.showMeMessage());

        try (Stream<String> stream = Files.lines(Paths.get("/file/does/not/exist"))) {
            stream.forEach(System.out::println);
        } catch (IOException ioex) {
            logger.log(Level.SEVERE, "Error message", ioex);
        }

        //debug
        if (logger.isLoggable(Level.FINE)) {
            logger.fine("Logger: In fine message");
        }
        if (logger.isLoggable(Level.FINEST)) {
            logger.finest("Logger: In finest message");
        }
        if (logger.isLoggable(Level.FINER)) {
            logger.finer(() -> "Logger: In finer message");
        }
        if (logger.isLoggable(Level.CONFIG)) {
            logger.fine("Logger: In config message");
        }
        if (logger.isLoggable(Level.ALL)) {
            logger.fine("Logger: In ALL message");
        }

        try {
            User userNull = new User();
            userNull.getName().toString();
        } catch (NullPointerException ex) {
            logger.warning("error message: " + ex.getMessage());
            logger.severe(() -> "fatal error message: " + ex.getMessage());
        }




    }
}
