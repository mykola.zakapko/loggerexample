package com.hillel.logger.core;

import com.hillel.logger.entity.User;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hello world!
 *
 */
public class Log4JApp
{
    private static final Logger rootLogger = LogManager.getRootLogger();
    private static final Logger userLogger = LogManager.getLogger(User.class);

    public static void main(String[] args) {
        User user = new User();
        user.setName("Peter");
        user.setLastName("Pupkin");

        userLogger.log(Level.ALL, user.showMeMessage());
        userLogger.info(user.giveMeASign());

        rootLogger.info("Root Logger: "  + user.showMeMessage());

        //debug
        if (rootLogger.isDebugEnabled()) {
            rootLogger.debug("RootLogger: In debug message");
            userLogger.debug("UserLogger in debug");
        }
        if (userLogger.isTraceEnabled())
        {
            userLogger.trace("UserLogger in trace");
        }

        try {
            User userNull = new User();
            userNull.getName().toString();
        } catch (NullPointerException ex) {
            userLogger.error("error message: " + ex.getMessage());
            userLogger.fatal("fatal error message: " + ex.getMessage());
        }


    }
}
