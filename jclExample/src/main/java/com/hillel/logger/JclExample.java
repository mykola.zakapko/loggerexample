package com.hillel.logger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

//https://habr.com/ru/post/247647/
public class JclExample
{
    static {
        String path = JclExample.class.getClassLoader()
                .getResource("commons-logging.properties")
                .getFile();
        System.setProperty("java.util.logging.config.file", path);
    }
    private static final Log log = LogFactory.getLog("JclExample");

    public static void main( String[] args )
    {
        Object object = "Сообщение";
        Throwable throwable = new Throwable();
        log.error(object);
        log.error(object, throwable);
    }
}
